# Getting the list of ips from specified urls
data "http" "ip_list" {
    url = "https://ip-ranges.atlassian.com/"
}

locals {
    # decode the json response and create variable with list of all ips
    ip_list = jsondecode(data.http.ip_list.body).items
}

# Output list of ipv4
# Assumed valid list of ipv4. Complex regex can be built to validate for that. Has been ommited. 
output "ipv4_list" {
    value = [
        for item in local.ip_list:
        item.cidr 
        if (length(regexall(":", item.cidr)) == 0)
    ]
}

# Null resource to facilitate execution every time
resource "null_resource" "dummy" {
    triggers = {
        always_run = "${timestamp()}"
    }
    provisioner "local-exec" {
        command = "true"
    }
}