# What it does ? 

Playing with terraform. The following tasks are being performed.

* Parse a list of valid json ipv4/v6 and output only the ipv4.

# How to run ? 

`terraform init`  
`terraform apply -auto-approve`